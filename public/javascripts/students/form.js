(function(){  
  $(function(){
    var students_form = {      
      form: null,
      form_wrapper: null,
      thank_you_wrapper: null,
      init: function() {
        this.initWrappers();
        this.initForm();
      },
      initWrappers: function() {
        this.form_wrapper = $('#form-wrapper');
        this.thank_you_wrapper = $('#thank-you');
        this.loading = $('#loading');
      },
      initForm: function() {
        var _self = this;
        this.form = $.fmx_form({
          container: $('#enroll-form'),
          loading: function() {
            console.log('loading');
            console.log(_self.loading);
            _self.loading.show();
          },
          loaded: function() {
            _self.loading.hide();
          },
          onSuccess: function(data) {
            if(data.enrolled) {
              _self.form_wrapper.hide();
              _self.thank_you_wrapper.fadeIn(200);
            }
          }
        });
      }
    };
    students_form.init();
  });
})(jQuery);
