var _zindex = 8000;
var fmx_current_dialog = null;
(function(){
	$.fmx_dialog = function(options) {
		var nil = $('');		
		var _self = fmx_current_dialog = {
			params: null,
			url: null,
			ajax: false,
			width: 400,
			min_height: 200,
			max_height: null,
			autoshow: true,
			overlay: null,
			html: null,
			body: null,
			loader: null,
			ajax_wrapper: null,
			show_close: true,
			color: "#FFFFFF",
			opacity: 0.7,
			title: null,
			msg: '',
			msg_container: nil,
			init: function(){
				_self.html = $('html');
				_self.body = $('body');				
				_self.initOverlay();				
				_self.renderMsg(_self.msg);				
			},
			initOverlay: function(){
				var s = '';
				s += '<div class="dialog_overlay"></div>';
				_self.overlay = $(s).css({
					"z-index": ++_zindex,
					display: "none",					
					"background-color": _self.color,
					opacity: _self.opacity,
					position: "fixed",
					top: 0,
					left: 0
				});
				_self.overlay.appendTo(_self.body);				
			},			
			renderMsg: function(msg){
				var s = '';
				s += '<div class="dialog_message_container">';
				    if(_self.title) {
				        s += '<div class="dialog_header">';
						    s += '<div class="dialog_title">'
							    s += '<h2>' + _self.title + '</h2>'
						    s += '</div>';
						    if(_self.show_close){
							    s += '<div class="dialog_close"><i class="icon icon-remove"></i></div>';	
						    }
						    s += '<div class="clear"></div>';
					    s += '</div>';
				    }					
					s += '<div class="dialog_body">';
						if(_self.ajax){
							s += '<div class="dialog_loader">';
							s += '<img src="/images/preloader.gif" alt="Loading..." />';
							s += '</div>';
							s += '<div class="dialog_ajax_wrapper"></div>';
						}
						else{
							s += msg;
						}
					s += '</div>';
				s += '</div>';
				_self.msg_container = $(s).css({
					"z-index": ++_zindex,
					"box-shadow": "0 0 10px #888a85",
					display: "none",
					width: _self.width,
					"min-height": _self.min_height,
					position: "fixed",
					top: "10%",
					left: "50%",
					"background-color": "#FFF",
					"margin-left": (_self.width/2)*(-1)					
				});
				if(_self.max_height != null){
					var dialog_body = _self.msg_container.find('.dialog_body');
					dialog_body.css({
						'max-height': _self.max_height,
						'overflow-y': 'scroll'
					});
				}
				_self.msg_container.appendTo(_self.body);
				if(_self.autoshow) {
					_self.show();
				}
				if(_self.show_close){
					_self.msg_container.find('.dialog_close').click(_self.destroy);	
				}
				if(_self.ajax){
					_self.loader = _self.msg_container.find('.dialog_loader');
					_self.ajax_wrapper = _self.msg_container.find('.dialog_ajax_wrapper');
					_self.doPost();
				}
			},
			show: function(){
				//_self.overlay.fadeIn(500);
				_self.overlay.show();
				_self.msg_container.show();
				//_self.msg_container.fadeIn(500);
				if(!_self.ajax){
					_self.onLoad(_self);
				}
			},
			onLoad: function(instance) {},	
			doPost: function() {
				_self.loader.show();
				$.ajax({
					type: 'get',
					url: _self.url,
					data: $.extend({'ajax_request' : true}, _self.params),
					error: function(){
						_self.onError();
					},
					success: function(data){
						_self.onSuccess(data);
					}
				});
			},			
			onError: function(){
				$.fmx_dialog({
					icon_src: 'error',
					msg: 'An error occured. Please try again later'
				});
			},
			onSuccess: function(data){
				_self.loader.hide();
				_self.ajax_wrapper.html(data);
				_self.onLoad(_self);
			},
			onClose: function(){},		
			destroy: function() {
				_self.msg_container.fadeOut(500,function(){
					_self.msg_container.remove();
					_self.overlay.fadeOut(500,function(){
						_self.overlay.remove();
						_self.onClose();
					});
				});				
			}
		};
		$.extend(_self,options);
		_self.init();
		return _self;
	}; 
})(jQuery);
