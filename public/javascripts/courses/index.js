(function(){
  $(function(){
    var certificatec = certificatec || {};
    certificatec.courses = {
      init: function() {
        this.checkHash();
      },
      checkHash: function() {
        if(window.location.hash) {
          this.scrollToCourse(window.location.hash.replace('#',''));
        }
      },
      scrollToCourse: function(id) {
        var new_top = $("[data-course=" + id + "]").position().top
        $('body, html').animate({
          scrollTop: new_top
        }, 500);
      }
    }  
    certificatec.courses.init();
  });
})(jQuery);
