(function(){
  $(function(){
    $.extend(certificatec.section_form, {
      initDatepicker: function() {
        $('#exam_date').datepicker({
          dateFormat: 'dd/mm/yy',
          minDate: 0
        });
      }
    });
    certificatec.section_form.initDatepicker();  
  });
})(jQuery);
