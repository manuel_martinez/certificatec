(function(){
  $(function(){
    $.extend(certificatec.section_form, {
      initDatepicker: function() {
        $('#course_date_date').datepicker({
          dateFormat: 'dd/mm/yy',
          minDate: 0
        });
      }
    });
    certificatec.section_form.initDatepicker();  
  });
})(jQuery);
