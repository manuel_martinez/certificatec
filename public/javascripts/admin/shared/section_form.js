(function(){  
  $(function(){
    certificatec.section_form = {      
      form: null,
      init: function() {
        this.initForm();
      },
      initForm: function() {
        this.form = $.fmx_form({
          container: $('#section-form'),
          onSuccess: function(data) {
            window.location = data.url;
          }
        });
      }
    };
    certificatec.section_form.init();
  });
})(jQuery);
