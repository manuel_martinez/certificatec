var certificatec = {
  csrf_params: {},
  init: function() {
    this.initCsrfParams();
    this.initTooltips();
    this.initLinks();
  },
  initCsrfParams: function() {
    var key = $('meta[name="csrf-param"]').attr('content');
    var value = $('meta[name="csrf-token"]').attr('content');
    this.csrf_params[key] = value;
  },
  initTooltips: function() {
    $('[data-tooltip]').tipsy({
      fade: true,
      gravity: 's',
      title: 'data-tooltip',
      opacity: 0.9
    });  
  },
  initLinks: function() {
    _self = this;
    $('a[data-method="delete"]').click(function(e){
      e.preventDefault();
      var url = $(this).attr('href') + '.json';
      var confirmation_message = $(this).attr('data-confirm') || "¿Seguro que deseas continuar?";
      _self.showConfirmDialog(confirmation_message, function(){
        _self.deleteAndReload(url);
      });
      
    });
  },
  showConfirmDialog: function(msg, confirm) {
    $.fmx_dialog({
      title: '¿Est&aacute;s seguro?',
      msg: this.confirmDialogBody(msg),
      onLoad: function(dialog) {
        dialog.msg_container.find('a.confirm-btn').click(function(e){
          e.preventDefault();
          confirm();
        });
        
        dialog.msg_container.find('a.cancel-btn').click(function(e){
          e.preventDefault();
          dialog.destroy();
        });
      }
    });
  },
  confirmDialogBody: function(msg) {
    var s = '<p class="no-margin-top">' + msg + '</p>';
    s += '<div class="actions align-right">';
      s += '<a href="#" class="btn gray-btn cancel-btn">Cancelar</a> ';
      s += '<a href="#" class="btn green-btn confirm-btn">Confirmar</a>';
    s += '</div>';
    return s;  
  },
  deleteAndReload: function(url) {
    var current_url = window.location;
    _self.doDelete(url, function() {
      window.location = current_url;
    });        
  },
  doDelete: function(url, callback) {
    var _self = this;
    $.ajax({
      type: 'delete',
      data: _self.csrf_params,
      url: url,
      success: function(data) {
        callback(data);
      },
      error: function() {
        _self.error();
      }        
    });
  }
};
