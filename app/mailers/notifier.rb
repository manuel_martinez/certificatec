class Notifier < ActionMailer::Base

  add_template_helper(ApplicationHelper)
  
  def enroll(student_id, group_id)
     @student = Student.find(student_id)
     @group = Group.find(group_id)
     include_logo
     mail(:to => @student.email, :subject => "Gracias por registrarte al curso", :from => "A01019671@itesm.mx")
  end
  
  private
    def include_logo
      attachments.inline['logo.png'] = File.read Rails.root.join('public','images','notifier','logo.png')     
    end
end
