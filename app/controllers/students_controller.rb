class StudentsController < ApplicationController
  def create
    @student = Student.new(params[:student])
    
    if @student.save
      enrollment = @student.enrollments.build(group_id: params[:group_id])
      enrolled = enrollment.save
      if enrolled
        Notifier.enroll(@student.id, params[:group_id]).deliver
      end 
    end
    
    respond_to do |format|
      format.json { render json: { errors: @student.errors, enrolled: enrolled } }
    end
  end
end
