class ApplicationController < ActionController::Base
  protect_from_forgery
  
  protected
    def get_current_user
      unless session[:user_id].blank?
        @current_user = User.select([:id, :name, :surname, :email]).find(session[:user_id])    
      end
    end
end
