class CoursesController < ApplicationController
  def index
    @courses = Course.active.order(:name)
  end
  
  def agenda
    @groups = Group.available.includes(:course_dates => :course, :exams => :course).order(:course_dates => :date)
  end
  
  def enroll
    @group = Group.find(params[:group_id])
    @student = Student.new
  end
end
