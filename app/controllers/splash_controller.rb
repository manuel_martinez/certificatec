class SplashController < ApplicationController
  def index
    @upcoming_dates = CourseDate.upcoming.includes(:course).where(:courses => {:is_active => 1}).limit(5)
    @upcoming_exam = Exam.upcoming.limit(1).first
  end
  
  def login
    render layout: 'login'
  end
  
  def logout
    reset_session
    flash[:notice] = 'Tu sesi&oacute;n ha terminado'
    redirect_to root_url
  end
  
  def authenticate
    @user = User.authenticate(params[:login][:email], params[:login][:password])
    if @user
      session[:user_id] = @user.id
      redirect_to admin_root_url
    else
      flash[:notice] = 'Usuario / Contrase&ntilde;a inv&aacute;lida'
      render :login, layout: 'login'
    end
  end
  
  def contact
    
  end
  
end
