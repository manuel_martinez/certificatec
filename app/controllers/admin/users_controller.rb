class Admin::UsersController < Admin::SharedController

  # GET /admin/users
  def index
    @users = User.active.exclude(@current_user).paginate(:page => params[:page]).order(:name)
  end
  
  # GET /admin/users/new
  def new
    @user = User.new
    render 'form'
  end
  
  # POST /admin/users.json
  def create
    password = params[:user].delete :password
    
    @user = User.new(params[:user])
    @user.password = password
    @user.save
    
    respond_to do |format|
      format.json { render json: { errors: @user.errors, url: admin_users_url } }
    end
  end
  
  # GET /admin/users/:id/edit
  def edit
    @user = User.active.find(params[:id])
    render 'form'    
  end
  
  # PUT /admin/users/:id.json
  def update    
    password = params[:user].delete :password
    
    @user = User.active.find(params[:id])
    @user.attributes = params[:user]
    @user.password = password unless password.blank?
    @user.save
    
    respond_to do |format|
      format.json { render json: { errors: @user.errors, url: admin_users_url } }
    end
  end
  
  # DELETE /admin/users/:id.json
  def destroy
    @user = User.active.find(params[:id])
    @user.is_active = 0
    
    respond_to do |format|
      format.json { render json: { deleted: @user.save } }
    end
  end

end
