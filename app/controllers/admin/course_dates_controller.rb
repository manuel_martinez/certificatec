class Admin::CourseDatesController < Admin::SharedController
  before_filter :get_course
  
  # GET /admin/courses/:course_id/course_dates
  def index 
    @course_dates = @course.course_dates.includes(:group).paginate(:page => params[:page]).order('date DESC')
  end
  
  # GET /admin/courses/:course_id/course_dates/:id
  def show
    @course_date = @course.course_dates.find(params[:id])
  end
  
  # GET /admin/courses/:course_id/course_dates/new
  def new
    @course_date = @course.course_dates.build
    @groups = Group.order(:name)
    render 'form'
  end
  
  # POST /admin/courses/:course_id/course_dates.json
  def create
    @course_date = @course.course_dates.build(params[:course_date])
    @course_date.save
    
    respond_to do |format|
      format.json { render json: { errors: @course_date.errors, url: admin_course_course_dates_url(@course) } }
    end
  end
  
  # GET /admin/courses/:course_id/course_dates/edit
  def edit
    @course_date = @course.course_dates.find(params[:id])
    @groups = Group.order(:name)
    render 'form'    
  end
  
  # PUT /admin/courses/:course_id/course_dates/:id.json
  def update
    @course_date = @course.course_dates.find(params[:id])
    @course_date.update_attributes(params[:course_date])
    
    respond_to do |format|
      format.json { render json: { errors: @course_date.errors, url: admin_course_course_dates_url(@course) } }
    end
  end
  
  # DELETE /admin/courses/:course_id/course_dates/:id.json
  def destroy
    @course_date = @course.course_dates.find(params[:id])
    
    respond_to do |format|
      format.json { render json: { deleted: @course_date.destroy } }
    end
  end
  
  protected
    def get_course
      @course = Course.active.find(params[:course_id])       
    end
end
