class Admin::ExamsController < Admin::SharedController
  before_filter :get_course
  
  # GET /admin/courses/:course_id/exams
  def index 
    @exams = @course.exams.paginate(:page => params[:page]).order('date DESC')
  end
  
  # GET /admin/courses/:course_id/exams/:id
  def show
    @exam = @course.exams.find(params[:id])
  end
  
  # GET /admin/courses/:course_id/exams/new
  def new
    @exam = @course.exams.build
    @groups = Group.order(:name)
    render 'form'
  end
  
  # POST /admin/courses/:course_id/exams.json
  def create
    @exam = @course.exams.build(params[:exam])
    @exam.save
    
    respond_to do |format|
      format.json { render json: { errors: @exam.errors, url: admin_course_exams_url(@course) } }
    end
  end
  
  # GET /admin/courses/:course_id/exams/edit
  def edit
    @exam = @course.exams.find(params[:id])
    @groups = Group.order(:name)
    render 'form'    
  end
  
  # PUT /admin/courses/:course_id/exams/:id.json
  def update
    @exam = @course.exams.find(params[:id])
    @exam.update_attributes(params[:exam])
    
    respond_to do |format|
      format.json { render json: { errors: @exam.errors, url: admin_course_exams_url(@course) } }
    end
  end
  
  # DELETE /admin/courses/:course_id/exams/:id.json
  def destroy
    @exam = @course.exams.find(params[:id])
    
    respond_to do |format|
      format.json { render json: { deleted: @exam.destroy } }
    end
  end
  
  protected
    def get_course
      @course = Course.active.find(params[:course_id])       
    end
end
