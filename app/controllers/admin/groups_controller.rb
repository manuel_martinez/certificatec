class Admin::GroupsController < Admin::SharedController

  # GET /admin/groups
  def index
    @groups = Group.order(:name).paginate(:page => params[:page])
  end
  
  # GET /admin/groups/new
  def new
    @group = Group.new
    render 'form'
  end
  
  # POST /admin/groups.json
  def create
    @group = Group.new(params[:group])
    @group.save
    
    respond_to do |format|
      format.json { render json: { errors: @group.errors, url: admin_groups_url } }
    end
  end
  
  # GET /admin/groups/:id/edit
  def edit
    @group = Group.find(params[:id])
    render 'form'    
  end
  
  # PUT /admin/groups/:id.json
  def update
    @group = Group.find(params[:id])
    @group.update_attributes(params[:group])
    
    respond_to do |format|
      format.json { render json: { errors: @group.errors, url: admin_groups_url } }
    end
  end
  
  # DELETE /admin/groups/:id.json
  def destroy
    @group = Group.find(params[:id])    
    
    respond_to do |format|
      format.json { render json: { deleted: @group.destroy } }
    end
  end
  
  def show
    @group = Group.find(params[:id])
    
    respond_to do |format|
      format.html
      format.csv { send_data @group.to_csv, :filename => "#{@group.name}.csv" }      
    end
  end

end
