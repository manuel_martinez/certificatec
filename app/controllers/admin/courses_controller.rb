class Admin::CoursesController < Admin::SharedController
  # GET /admin/courses
  def index
    @courses = Course.active.order(:name).paginate(:page => params[:page])
  end
  
  # GET /admin/courses/new
  def new
    @course = Course.new
    render 'form'
  end
  
  # POST /admin/courses.json
  def create
    @course = Course.new(params[:course])
    @course.save
    
    respond_to do |format|
      format.json { render json: { errors: @course.errors, url: admin_courses_url } }
    end
  end
  
  # GET /admin/courses/:id/edit
  def edit
    @course = Course.active.find(params[:id])
    render 'form'    
  end
  
  # PUT /admin/courses/:id.json
  def update
    @course = Course.active.find(params[:id])
    @course.update_attributes(params[:course])
    
    respond_to do |format|
      format.json { render json: { errors: @course.errors, url: admin_courses_url } }
    end
  end
  
  # DELETE /admin/courses/:id.json
  def destroy
    @course = Course.active.find(params[:id])
    @course.is_active = 0
    
    respond_to do |format|
      format.json { render json: { deleted: @course.save } }
    end
  end
end
