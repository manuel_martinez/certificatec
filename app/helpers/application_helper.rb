module ApplicationHelper
  MONTHS = [
    {:short => 'Ene', :long => 'Enero'},
    {:short => 'Feb', :long => 'Febrero'},
    {:short => 'Mar', :long => 'Marzo'},
    {:short => 'Abr', :long => 'Abril'},
    {:short => 'May', :long => 'Mayo'},
    {:short => 'Jun', :long => 'Junio'},
    {:short => 'Jul', :long => 'Julio'},
    {:short => 'Ago', :long => 'Agosto'},
    {:short => 'Sep', :long => 'Septiembre'},
    {:short => 'Oct', :long => 'Octubre'},
    {:short => 'Nov', :long => 'Noviembre'},
    {:short => 'Dic', :long => 'Diciembre'}
  ]

  def slash_format(date)
    date.strftime("%d/%m/%Y")
  end
  
  def text_format(date)
    "#{date.day} de #{MONTHS[date.month][:long]} de #{date.year}"
  end
  
  def short_month(date)    
    MONTHS[date.month - 1][:short]
  end
  
  def time_format(date)
    date.strftime("%H:%M")
  end
  
  def hour_of_day(date)
    date.strftime("%I")
  end
  
  def long_month(date)
    MONTHS[date.month - 1][:long]
  end
  
  def hour_options
    (0..23).to_a
  end
  
  def minute_options
    (0..60).to_a
  end
  
  def nav_link(text, path)
    class_name =  current_page?(path) ? 'active' : ''
    link_to text, path, :class => class_name
  end
  
  def render_table_row(index)
    if index.odd?
      content_tag :tr, :class => 'alt' do
          yield
      end
    else
      content_tag :tr do
          yield
      end
    end
  end
  
  def paginate(collection)
    will_paginate collection, :previous_label => '&#8592; Anterior', :next_label => 'Siguiente &#8594;'
  end
  
  def group_options(groups)
    groups.map do |group|
      [group.name, group.id]
    end
  end
end
