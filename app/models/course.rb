class Course < ActiveRecord::Base
  has_many :course_dates
  has_many :exams
  
  validates_presence_of :name
  validates_length_of :name, :in => 2..100
  
  scope :active, where(:is_active => 1)
  
  def available_dates
    self.course_dates.where('date > ?', Date.today)
  end
  
end
