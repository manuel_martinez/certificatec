class CourseDate < ActiveRecord::Base
  belongs_to :group
  belongs_to :course
  
  scope :upcoming, where('date > ?', Date.today).order(:date)
  
end
