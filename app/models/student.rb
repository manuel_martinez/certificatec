class Student < ActiveRecord::Base
  has_many :enrollments
  
  CSV_COLUMNS = ["Nombre","Apellidos","Email","Empresa","Telefono"]
  
  validates_presence_of :name, :surname, :email, :company
  
  # Returns self's full_name  
  def full_name
    "#{self.name} #{self.surname}"
  end 
  
  def to_csv_array
    [self.name, self.surname, self.email, self.company, self.phone]
  end
end
