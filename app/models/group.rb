class Group < ActiveRecord::Base
  has_many :enrollments
  has_many :course_dates
  has_many :exams
  has_many :students, :through => :enrollments
  
  validates_presence_of :name, :price, :start_date
  validates_numericality_of :price
  
  scope :available, where('start_date > ?', Date.today)
  
  def to_csv
    require 'csv'
    CSV.generate do |csv| 
      csv << [self.name] << []
      csv << Student::CSV_COLUMNS
      self.students.each do |student|
        csv << student.to_csv_array
      end
    end
  end
end
