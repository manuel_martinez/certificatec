class Exam < ActiveRecord::Base

  belongs_to :course
  belongs_to :group

  validates_presence_of :date, :location  
  
  scope :upcoming, where('date > ?', Time.now).order(:date)
  
  def minutes
    self.date.min if self.date
  end
  
  def minutes=(minutes)
    self.date = self.date.change(:min => minutes)
  end
  
  def hour
    self.date.hour if self.date
  end
  
  def hour=(hour)
    self.date = self.date.change(:hour => hour)
  end
end
