# Certificatec


## General Info
Ruby 1.9.2
Rails 3.0.19
MySQL

## Deployment
1. Install all gems

        bundle install    

2. Update db/config.yml

3. Make sure seeds are in place  

        rake db:seed RAILS_ENV=production   
    
    
4. Make sure mailer settings are correct for the environment (check config/environments/development.rb) for an example  

5. Start background processing  

        script/delayed_job start  

6. Enjoy!

