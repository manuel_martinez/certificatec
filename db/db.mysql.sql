
CREATE TABLE delayed_jobs (
                id INT AUTO_INCREMENT NOT NULL,
                priority INT DEFAULT 0 NOT NULL,
                attempts INT DEFAULT 0 NOT NULL,
                handler TEXT NOT NULL,
                last_error TEXT,
                run_at DATETIME,
                locked_at DATETIME,
                failed_at DATETIME,
                locked_by VARCHAR(255),
                queue VARCHAR(255),
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE groups (
                id BIGINT AUTO_INCREMENT NOT NULL,
                name VARCHAR(50) NOT NULL,
                price DOUBLE PRECISION NOT NULL,
                start_date DATE NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE users (
                id BIGINT AUTO_INCREMENT NOT NULL,
                name VARCHAR(20) NOT NULL,
                surname VARCHAR(50) NOT NULL,
                email VARCHAR(50) NOT NULL,
                encrypted_password VARCHAR(100) NOT NULL,
                is_active INT DEFAULT 1 NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE students (
                id BIGINT AUTO_INCREMENT NOT NULL,
                name VARCHAR(50) NOT NULL,
                surname VARCHAR(50) NOT NULL,
                email VARCHAR(50) NOT NULL,
                company VARCHAR(50) NOT NULL,
                phone VARCHAR(30) NOT NULL,
                updated_at DATETIME NOT NULL,
                created_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE enrollments (
                id BIGINT AUTO_INCREMENT NOT NULL,
                student_id BIGINT NOT NULL,
                group_id BIGINT NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE courses (
                id BIGINT AUTO_INCREMENT NOT NULL,
                name VARCHAR(100) NOT NULL,
                description TEXT NOT NULL,
                is_active INT DEFAULT 1 NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE exams (
                id BIGINT AUTO_INCREMENT NOT NULL,
                date DATETIME NOT NULL,
                location TEXT NOT NULL,
                group_id BIGINT,
                course_id BIGINT NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE course_dates (
                id BIGINT AUTO_INCREMENT NOT NULL,
                date DATE NOT NULL,
                course_id BIGINT NOT NULL,
                group_id BIGINT,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE contact_requests (
                id BIGINT AUTO_INCREMENT NOT NULL,
                name VARCHAR(50) NOT NULL,
                surname VARCHAR(50) NOT NULL,
                email VARCHAR(50) NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME NOT NULL,
                PRIMARY KEY (id)
);


ALTER TABLE enrollments ADD CONSTRAINT groups_enrollments_fk
FOREIGN KEY (group_id)
REFERENCES groups (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE course_dates ADD CONSTRAINT groups_course_dates_fk
FOREIGN KEY (group_id)
REFERENCES groups (id)
ON DELETE SET NULL
ON UPDATE SET NULL;

ALTER TABLE exams ADD CONSTRAINT groups_exams_fk
FOREIGN KEY (group_id)
REFERENCES groups (id)
ON DELETE SET NULL
ON UPDATE SET NULL;

ALTER TABLE enrollments ADD CONSTRAINT students_enrollments_fk
FOREIGN KEY (student_id)
REFERENCES students (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE course_dates ADD CONSTRAINT courses_course_dates_fk
FOREIGN KEY (course_id)
REFERENCES courses (id)
ON DELETE CASCADE
ON UPDATE CASCADE;

ALTER TABLE exams ADD CONSTRAINT courses_exams_fk
FOREIGN KEY (course_id)
REFERENCES courses (id)
ON DELETE CASCADE
ON UPDATE CASCADE;
