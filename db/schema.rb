# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130731002904) do

  create_table "contact_requests", :force => true do |t|
    t.string   "name",       :limit => 50, :null => false
    t.string   "surname",    :limit => 50, :null => false
    t.string   "email",      :limit => 50, :null => false
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "course_dates", :force => true do |t|
    t.date     "date",                    :null => false
    t.integer  "course_id",  :limit => 8, :null => false
    t.integer  "group_id",   :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "course_dates", ["course_id"], :name => "courses_course_dates_fk"
  add_index "course_dates", ["group_id"], :name => "groups_course_dates_fk"

  create_table "courses", :force => true do |t|
    t.string   "name",        :limit => 100,                :null => false
    t.text     "description",                               :null => false
    t.integer  "is_active",                  :default => 1, :null => false
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "enrollments", :force => true do |t|
    t.integer  "student_id", :limit => 8, :null => false
    t.integer  "group_id",   :limit => 8, :null => false
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "enrollments", ["group_id"], :name => "groups_enrollments_fk"
  add_index "enrollments", ["student_id"], :name => "students_enrollments_fk"

  create_table "exams", :force => true do |t|
    t.datetime "date",                    :null => false
    t.text     "location",                :null => false
    t.integer  "group_id",   :limit => 8
    t.integer  "course_id",  :limit => 8, :null => false
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "exams", ["course_id"], :name => "courses_exams_fk"
  add_index "exams", ["group_id"], :name => "groups_exams_fk"

  create_table "groups", :force => true do |t|
    t.string   "name",       :limit => 50, :null => false
    t.float    "price",                    :null => false
    t.date     "start_date",               :null => false
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "students", :force => true do |t|
    t.string   "name",       :limit => 50, :null => false
    t.string   "surname",    :limit => 50, :null => false
    t.string   "email",      :limit => 50, :null => false
    t.string   "company",    :limit => 50, :null => false
    t.string   "phone",      :limit => 30, :null => false
    t.datetime "updated_at",               :null => false
    t.datetime "created_at",               :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name",               :limit => 20,                 :null => false
    t.string   "surname",            :limit => 50,                 :null => false
    t.string   "email",              :limit => 50,                 :null => false
    t.string   "encrypted_password", :limit => 100,                :null => false
    t.integer  "is_active",                         :default => 1, :null => false
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
  end

end
