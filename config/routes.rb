Certificatec::Application.routes.draw do
  root :to => 'splash#index'
  
  match '/login' => 'splash#login', :as => 'login'
  match '/logout' => 'splash#logout', :as => 'logout'
  match 'splash/authenticate' => 'splash#authenticate', :via => :post
  match '/cursos' => 'courses#index', :as => 'courses'
  match '/fechas' => 'courses#agenda', :as => 'agenda'
  match '/inscripcion/:group_id' => 'courses#enroll', :as => 'enroll'
  match '/contacto' => 'splash#contact', :as => 'contact'
  
  resources :students, :only => :create
  
  namespace :admin do 
    root :to => 'courses#index'
    
    resources :courses do
      resources :course_dates
      resources :exams
    end
    resources :groups
    resources :users
  end
end
